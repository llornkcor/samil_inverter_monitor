#!/bin/bash

luxnow=`su pi -c /home/pi/light-sensor.py`
currentdatetime=`date +"%d%m%y%H%M"`
logfile=/var/log/inverter.log

function powersave() {
echo "Powersave mode, lux: $luxnow $currentdatetime" >> $logfile
	/usr/sbin/rfkill block wifi
}

function unpowersave() {
	echo "unpowersave, lux: $luxnow $currentdatetime" >> $logfile
	/usr/sbin/rfkill unblock wifi
}

function isPowerSaveMode() {
	if [ "$STATE" = "blocked" ]; then
		return 1;
	fi
return 0 
}

STATE=$(/usr/sbin/rfkill -n --output SOFT list wifi)
echo "$STATE: $luxnow lux $currentdatetime" >> $logfile

if [ $luxnow -gt 115 ]; then
	# daylight, check if wifi is blocked
	if [ "$STATE" = "blocked" ]; then
		# need to unblock wifi
		unpowersave
	fi
else 
	# dark, check is unblocked
	if [ "$STATE" = "unblocked" ]; then
		powersave
	fi
	exit 1 # just bail, inverter is not on anyway
fi

# /usr/sbin/rfkill unblock wifi
/usr/bin/perl /inverter_monitor-master/inverter.pl /dev/ttyUSB0 > cat /solar/current.txt >> /solar/output/`date +%d%m%y`

# rfkill -n --output SOFT list wifi 
# unblocked
